This is a rust application for testing how deep the recursion can be accepted in main thread of a typical rust application.

## Why

Like most programming languages, rust uses stacks for function call and local variable allocation. It's reasonable to know how much stack you can use in default.

## Bad news

As of now (March, 2017), it's not possible to adjust stack size of main thread in a rust application. Thus, if you need larger stack, you may:

* Use `Box` for allocating data on the heap
* Run recursive code in another thread (See http://stackoverflow.com/a/29980945)

In this code, I showed how to spawn a thread with custom stack size.

## Run

To run this application, you may use `cargo run`, or run compiled binary manually.

To run recursion on main thread:

```sh
# With Cargo
cargo run

# Run the binary
stack_size
```

To run recursion on spawned thread with 10 MB stack size:

```sh
# With Cargo
cargo run -- -t 10

# Run the binary
stack_size -t 10
```

The application panics eventually, like this:

```
( Omitted ...)
47619
47620
47621
47622

thread 'thread_for_recurse' has overflowed its stack
fatal runtime error: stack overflow
```

In the above example, the stack space is enough for 47622 recursions.

## Interesting fact

* Running `cargo run` yeilds about 47.5k recursions. The number varies for each run.
* Running `cargo run -- -t 8` yeilds 47622 recursions for each run.

Maybe on my system, default stack size of main thread is about 8 MB?

```
$ rustc --version
rustc 1.15.1 (021bd294c 2017-02-08)

$ uname -r
4.4.49-16-default

$ lsb_release -a    
LSB Version:	n/a
Distributor ID:	openSUSE project
Description:	openSUSE Leap 42.2
Release:	42.2
Codename:	n/a
```
