use std::thread;
use std::env;
use std::error::Error;

fn main() {
    let args: Vec<String> = env::args().collect();
    
    if args.len() >= 2 && args[1] == "-h" {
        print_help();
        return;
    } else if args.len() >= 3 && args[1] == "-t" {
        let stack_size_parse_result = args[2].parse::<usize>();
        match stack_size_parse_result {
            Err(e) => {
                print!("Error when parsing argument: {}", e.description());
            },
            Ok(stack_size) => {
                recurse_with_thread(stack_size * 1024 * 1024);
            }
        }
    } else {
        recurse_without_thread();
    }
}

fn recurse_without_thread() {
    recur(1);
}

#[allow(unused_must_use)]
fn recurse_with_thread(size: usize) {
    let thread_name = "thread_for_recurse".to_string();
    let t = thread::Builder::new().name(thread_name).stack_size(size).spawn(|| {
        recur(1);
    });
    
    match t {
        Err(e) => {
            print!("Error when spawning: {}", e.description());
        },
        Ok(join_handle) => {
            join_handle.join();
        }
    };
}

#[allow(unconditional_recursion)]
fn recur(i: u32) {
    println!("{}", i);
    recur(i + 1);
}

fn print_help() {
    println!("To run recursion in main thread:");
    println!("  stack_size");
    println!("");
    println!("To run recursion in another thread:");
    println!("  stack_size -t [size]");
    println!("  where `size` is stack size (in MB, or 1024 * 1024 bytes) of new thread");
}
